FROM python:3.11-slim

RUN apt-get -qq update \
    && apt-get -qq --no-install-recommends install ssh wget sshpass \
    && pip install -q --disable-pip-version-check ansible \
    && ansible-galaxy collection install community.sops \
    && wget -q https://github.com/getsops/sops/releases/download/v3.7.3/sops-v3.7.3.linux.amd64 -O /usr/local/bin/sops \
    && chmod +x /usr/local/bin/sops \
    && wget -q https://alxrem-files-repo.website.yandexcloud.net/openbao/v2.0.1+c65d9ebc3+yckms/linux_amd64/bao -o /usr/local/bin/bao \
    && chmod +x /usr/local/bin/bao \
    && find /var/lib/apt -type f -delete
